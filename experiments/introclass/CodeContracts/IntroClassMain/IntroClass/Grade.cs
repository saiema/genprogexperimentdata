﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroClass
{
    class Grade
    {

        public static int GradeImplementation(double a, double b, double c, double d, double score)
        {
            if (score >= a)
            {
                return 1;
            }
            if (score < a && score >= b)
            {
                return 2;
            }
            if (score < b && score >= c)
            {
                return 3;
            }
            if (score < c && score >= d)
            {
                return 4;
            }
            return 5;
        }


        /**
         * Given 4 values representing the threshold for grades A(1), B(2), C(3), and D(4)
         * it takes a fifth value representing a students score and returns a in the grade according to the previous thresholds
         * Returned grades are : 1 for A, 2 for B, 3 for C, 4 for D, and 5 for F.
         */
        public static int GradeWrapper(double a, double b, double c, double d, double score)
        {
            Contract.Requires(a > b && b > c && c > d && d >= 0 && score >= 0);
            Contract.Ensures(
                                (score >= a && Contract.Result<int>() == 1)
                                ||
                                (score < a && score >= b && Contract.Result<int>() == 2)
                                ||
                                (score < b && score >= c && Contract.Result<int>() == 3)
                                ||
                                (score < c && score >= d && Contract.Result<int>() == 4)
                                ||
                                (score < d && Contract.Result<int>() == 5)
                            );
            return GradeImplementation(a, b, c, d, score);
        }

    }
}
