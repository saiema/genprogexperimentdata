﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroClass
{
    class Median
    {

        public static int MedianImplementation(int a, int b, int c)
        {
            int frst,
                scnd,
                thrd,
                cmp1,
                cmp2,
                med;

            
            frst = a;
            scnd = b;
            thrd = c;

            if (frst <= scnd)
                cmp1 = frst;
            else cmp1 = scnd;
            if (scnd <= thrd)
                cmp2 = scnd;
            else cmp2 = thrd;
            if (cmp1 >= cmp2)
                med = cmp1;
            else med = cmp2;

            return med;

        }


        public static int MedianWrapper(int a, int b, int c)
        {
            //(a,b,c), (a,c,b), (b,a,c), (b,c,a), (c,a,b), (c,b,a)
            //a > b (c,b,a), (b,c,a), (b,a,c)
                //b > c (c,b,a) return b
                //b <= c (b,c,a), (b,a,c)
                    //a > c (b,c,a) return c
                    //a <= c (b,a,c) return a
            //a <= b (a,b,c), (a,c,b), (c,a,b)
                //b > c (a,c,b), (c,a,b)
                    //a > c (c,a,b) return a
                    //a <= c (a,c,b) return c
                //b <= c (a,b,c) return b
            Contract.Ensures(
                                (a > b && b > c && Contract.Result<int>() == b) 
                                || 
                                (a > b && b <= c && a > c && Contract.Result<int>() == c) 
                                || 
                                (a > b && b <= c && a <= c && Contract.Result<int>() == a)
                                || 
                                (a <= b && b > c && a > c && Contract.Result<int>() == a) 
                                || 
                                (a <= b && b > c && a <= c && Contract.Result<int>() == c) 
                                || 
                                (a <= b && b <= c && Contract.Result<int>() == b)
                                );
            return MedianImplementation(a, b, c);
        }

    }
}
