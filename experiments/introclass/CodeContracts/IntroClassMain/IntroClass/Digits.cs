﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroClass
{
    class Digits
    {

        public static int[] DigitsImplementation(int input)
        {
            int mult = input < 0 ? -1 : 1;
            int size = input < 0 ? (input.ToString().Length - 1) : input.ToString().Length;
            int[] result = new int[size];
            int i = 0;
            while (input >= 10 || input <= -10)
            {
                int digit = input % 10;
                result[i] = digit * mult;
                i++;
                input = input / 10;
            }
            result[i] = input * mult;
            return result;

        }

        /**
         * Given an int value
         * Returns an array of int values representing all digits in the input value ordered from less significant digit to most significant
         */
        public static int[] DigitsWrapper(int input)
        {
            Contract.Requires(input != int.MinValue);
            Contract.Ensures(
                                    (input < 0 && input.ToString().Length - 1 == Contract.Result<int[]>().Length)
                                    ||
                                    (input >= 0 && input.ToString().Length == Contract.Result<int[]>().Length)
                            );
            Contract.Ensures(
                                (input < 0 && Contract.ForAll(0, input.ToString().Length - 2, i => Contract.Result<int[]>()[i] == input.ToString()[input.ToString().Length - 1 - i] - 48)
                                       && (input.ToString()[1] - 48) == Contract.Result<int[]>()[input.ToString().Length - 2] * -1)
                                ||
                                (input >= 0 && Contract.ForAll(0, input.ToString().Length, i => Contract.Result<int[]>()[i] == input.ToString()[input.ToString().Length - 1 - i] - 48))
                            );
            return DigitsImplementation(input);
        }
            return DigitsImplementation(input);
        }

    }
}
