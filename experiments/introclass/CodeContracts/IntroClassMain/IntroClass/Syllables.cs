﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IntroClass
{
    class Syllables
    {

        public static unsafe int SyllablesImplementation(string input)
        {
            int numVowels = 0;
            int i;
            input = input.ToLower();
            for (i = 0; i < input.Length; i++)
            {
                if (input[i] == 'a' || input[i] == 'e' || input[i] == 'i' || input[i] == 'o' || input[i] == 'u' || input[i] == 'y')
                    numVowels += 1;
            }

            return numVowels;
        }

        /**
         * Given a string with at most 20 characters
         * Counts how many vowels ((a, e, i, o, u, y) there is in the input string
         */
        public static unsafe int SyllablesWrapper(string input)
        {
            Contract.Requires(input != null);
            Contract.Requires(0 <= input.Length && input.Length <= 20);
            Contract.Ensures(input.Count(x => (x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u' || x == 'y')) == Contract.Result<int>());
            return SyllablesImplementation(input);
        }



    }
}
