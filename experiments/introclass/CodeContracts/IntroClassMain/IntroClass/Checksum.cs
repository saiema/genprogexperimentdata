﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroClass
{
    class Checksum
    {

        public static char ChecksumImplementation(string input)
        {
            char n;     //**/
            int total = 0;  //**/


            int i = 0;
            while (i < input.Length)
            {

                n = input[i];
                total += (int)n;
                i++;

            }

            total = (total % 64) + 32;
            char output = (char)total;

            return output;
        }

        public static char ChecksumWrapper(string input)
        {
            Contract.Requires(input != null);
            Contract.Ensures(((input.Sum(x => x) % 64) + 32) == Contract.Result<char>());
            return ChecksumImplementation(input);
        }

    }
}
