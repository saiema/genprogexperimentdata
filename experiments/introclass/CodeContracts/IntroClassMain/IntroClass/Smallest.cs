﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroClass
{
    class Smallest
    {

        public static int SmallestImplementation(int a, int b, int c, int d)
        {

            if (a <= b && a <= c && a <= d)
            {
                return a;
            }
            else if (b <= a && b <= c && b <= d)
            {
                return b;
            }
            else if (c <= a && c <= b && c <= d)
            {
                return c;
            }
            else if (d <= a && d <= b && d <= c)
            {
                return d;
            }
            return 0;
        }

        /**
         * Given 4 numbers, return the smaller value 
         */
        public static int SmallestWrapper(int a, int b, int c, int d)
        {
            Contract.Ensures(Contract.Result<int>() == a || Contract.Result<int>() == b || Contract.Result<int>() == c || Contract.Result<int>() == d);
            Contract.Ensures(Contract.Result<int>() <= a && Contract.Result<int>() <= b && Contract.Result<int>() <= c && Contract.Result<int>() <= d);
            return SmallestImplementation(a, b, c, d);
        }

    }
}
