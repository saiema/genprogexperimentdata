#!/bin/bash


########MAIN ARGUMENTS########
#$1 : directory to run
#$2 : name of the program to repair without extension
#$3 : testing script
########GENPROG ARGUMENTS########
#$4 : --appp (default 0.33333)
#$5 : --compiler-opts (default "")
#$6 : --delp (default 0.33333)
#$7 : --keep-source (default false, no flag)
#$8 : --neg-weight (default 1)
#$9 : --pos-weight (default 0.1)
#$10 : --no-rep-cache (default false, no flag)
#$11 : --repp (default 0)
#$12 : --search (default "brute")
#$13 : --swapp (default 0.33333)
########EXTRA ARGUMENTS########
#$14 : Genprog runs
#$15 : extra flags

function push() {
	while [ 1 ]; do
		git pull
		ecode="$?"		
		if [[ "$ecode" -ne "0" ]]; then
			echo "Failed to pull with error $ecode"
			exit 2
		fi
		git push
		ecode="$?"
		if [[ "$ecode" -eq "0" ]]; then
			break
		elif [[ "$ecode" -eq "1" ]]; then		
			echo "git pull requires a pull.... retrying"		
		else
			echo "Push failed with error $ecode"
			exit 2		
		fi
	done
}

USEGIT=0
GENPROG="/home/stein/Downloads/genprog-source-v3.0/src/repair"
#GENPROG=~/Escritorio/GenProg/GenProg/genprog-source-v3.0/src/repair (en VISION funciona con este)
USEEXTERNALOUT=1
OUTPUT="/root/tests-vs-specs/experiments/introclass/genprog/output/"

dir=$1

cd $dir

varDir=$(echo $dir | sed "s|../IntroClass/$2/||g")
ID=${varDir:0:7}-${varDir:(-3)}
variantID="$2-$ID"

testingScript=$3


if [ ! -e $testingScript ]; then
	echo "Missing testing script: $testingScript"	
	exit 1
fi

posTests=$(grep -c  "^p[0-9]" $testingScript)
negTests=$(grep -c  "^n[0-9]" $testingScript)

#echo "Positive tests: $posTests"
#echo "Negative tests: $negTests"

if [[ ! $testingScript == *\.sh ]]; then
	echo "Testing script is not a script"
	exit 1
fi

if [[ $posTests == 0 && $negTests == 0 ]]; then
	echo "Incorrect script format, maybe you want to use runGenProg.sh?"
	exit 1
fi

testType=""
if [[ $testingScript =~ blackbox_* ]]; then
	testType="BB"
elif [[ $testingScript =~ whitebox_* ]]; then
	testType="WB"
else
	testType="Custom"
fi

experimentsFile="experiments"$testType".txt"
if [[ "$USEEXTERNALOUT" -eq "1" ]]; then
	experimentsFile="$OUTPUT$2/$ID/$experimentsFile"
	mkdir -p "$OUTPUT$2/$ID"
fi

rm -f $experimentsFile

testingScriptFlag="--test-script ./"$3
genProgRuns=${14}
#genProgRuns=1
apppFlag=" --appp "$4
compilerOptsValue=$5
options="-fno-stack-protector"
if [[ !  -z  $compilerOptsValue  ]]; then
	options="$options $compilerOptsValue"
fi
export GCCOPTIONS="$options"
compilerOptsFlag=" --compiler-opts \$GCCOPTIONS"
delpFlag=" --delp "$6
keepSourceValue=$7
keepSourceFlag=" "
if [[ $keepSourceValue ]]; then
	keepSourceFlag=" --keep-source"
fi
negWeightFlag=" --neg-weight "$8
posWeightFlag=" --pos-weight "$9
noRepCacheValue=${10}
noRepCacheFlag=" "
if [[ $noRepCacheValue ]]; then
	noRepCacheFlag=" --no-rep-cache"
fi
reppFlag=" --repp "${11}
searchFlag=" --search "${12}
swappFlag=" --swapp "${13}
extraflags="${@:15}"
#extraflags=""

for (( count=1; count<=$genProgRuns; count++ ))
do

	#CLEANING

	rm -f repair.*
	rm -f coverage*
	rm -f *.cache
	rm -f *.i
	rm .genprog_test_cache.json
	find . -regex '.*/[0-9]+' | xargs -I {} rm {}
	find . -regex '.*/[0-9]+\.c' | xargs -I {} rm {}

	#GENERATE .i files for every .c file

	for f in $(ls | egrep '\.c$'); do
		gcc -E $f -lm -fno-stack-protector >> "${f/.c/.i}"
	done

	#RUN GENPROG

	#pattern="Main*.i"
	#program=$( ls *${pattern}* )
	program=$2".i"
	
	genprog="$GENPROG"
	faultFlag=""
	if [ -e "fault.txt" ]; then
		faultFlag="--fault-file fault.txt"
	fi

	genProgFlags="$apppFlag $compilerOptsFlag $delpFlag $keepSourceFlag $negWeightFlag $posWeightFlag $noRepCacheFlag $reppFlag $searchFlag $swappFlag"	

	command="$genprog --program $program $testingScriptFlag --pos-tests $posTests --neg-tests $negTests $genProgFlags $faultFlag $extraflags"
	echo "About to run command : $command"
	START=$(date +%s.%N)
	timeout --signal=9 3h $command
	exitCode="$?"
	END=$(date +%s.%N)
	experimentTime=$(echo "$END - $START" | bc)

	repairFile="repair.c"
	minimizationDir="Minimization_Files/"
	repairDir="repair$count$testType/"
	if [[ "$USEEXTERNALOUT" -eq "1" ]]; then
		repairDir="$OUTPUT$2/$ID/$repairDir"
	fi

	repairFound=0

	if [ -e $repairFile ]
	then
	  	mkdir -p $repairDir
		mv $repairFile $repairDir
		mv $minimizationDir $repairDir
		if [[ "$USEGIT" -eq "1" ]]; then
			git add "$repairDir"
		fi
		repairFound=1
		echo "repair found"
	else
		echo "repair not found"
	fi
	experimentTimeOutput="$experimentTime"
	if [[ "$exitCode" -eq "124" ]] || [[ "$exitCode" -eq "137" ]]; then
		experimentTimeOutput="$experimentTimeOutput (TO)"
	else
		experimentTimeOutput="$experimentTimeOutput (exit code : $exitCode)"
		if [[ "$repairFound" -eq "1" ]]; then
			experimentTimeOutput="$experimentTimeOutput [REPAIR FOUND]"
		fi
	fi
	echo "Experiment $count finished in $experimentTimeOutput" >> $experimentsFile
	
	if [[ "$USEGIT" -eq "1" ]]; then
		git add $experimentsFile
		git commit -m "Experiment $variantID run $count"
		push
	fi

	#CLEANING
	rm -f repair.*
	rm -f coverage*
	rm -f *.cache
	rm -f *.i
	rm .genprog_test_cache.json
	find . -regex '.*/[0-9]+' | xargs -I {} rm {}
	find . -regex '.*/[0-9]+\.c' | xargs -I {} rm {}

done
