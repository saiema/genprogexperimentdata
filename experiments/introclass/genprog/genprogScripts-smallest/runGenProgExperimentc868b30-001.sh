#!/bin/bash

########MAIN ARGUMENTS########
#$1 : directory to run
#$2 : name of the program to repair without extension
#$3 : testing script
########GENPROG ARGUMENTS########
#$4 : --appp (default 0.33333)
#$5 : --compiler-opts (default "")
#$6 : --delp (default 0.33333)
#$7 : --keep-source (default false, no flag)
#$8 : --neg-weight (default 1)
#$9 : --pos-weight (default 0.1)
#$10 : --no-rep-cache (default false, no flag)
#$11 : --repp (default 0)
#$12 : --search (default "brute")
#$13 : --swapp (default 0.33333)
########EXTRA ARGUMENTS########
#$14 : Genprog runs
#$15 : extra flags

genProgRuns=10
extraflags="--minimization --ignore-dead-code --ignore-standard-headers --ignore-equiv-appends --label-repair"
args="0.5 -lm 0.85 1 0.35 0.65 1 0.75 ga 0 "$genProgRuns

program="smallest"
script="test.sh"
programDir="../IntroClass/$program"
fullArgs="$program $script $args $extraflags"

./runGenProg_introClass.sh $programDir/c868b30a4adebf62b0ed20170a14ee9e5f8bc62d827e9712294ffa4a10ab8423e3d903c29e2392c83963972019a470e667c1987e2547294d1e2d1df1db832912/001 $fullArgs
