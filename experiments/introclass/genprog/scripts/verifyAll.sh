# Run the given tests on every patch. If any test fails a file named "spurious" is created
# otherwise a file named "candidate" is created

find ../output -name "repair*Custom" | while read line; do

  project=$(echo $line | cut -d'/' -f 3)
  userShort=$(echo $line | cut -d'/' -f 4 | cut -d'-' -f 1)
  user=$(ls ../IntroClass/$project | grep "^$userShort")
  version=$(echo $line | cut -d'/' -f 4 | cut -d'-' -f 2)
  repair=$(echo $line | cut -d'/' -f 5)
  
  cmd="./verifyRepairVersion.sh $project $user $version $repair 1"
  echo "$cmd"
  ./verifyRepairVersion.sh $project $user $version $repair 1
  
  aux=$?
  if [[ $aux = 0 ]]; then
    touch "$line/candidate"
  elif [[ $aux = 1 ]]; then
    touch "$line/spurious"
  else
    echo "FAIL: $aux"
    echo "#2 : missing repair.c or testing script"
    echo "#3 : script format error in testing script"
    echo "#4 : errors setting up temporary working dir"
    echo "$cmd"
  fi
done
