int nondet_int();

int main_inner(int cbmc_a, int cbmc_b, int cbmc_c);
int main(){

  int cbmc_a = nondet_int();
  int cbmc_b = nondet_int();
  int cbmc_c = nondet_int();

  int r = main_inner(cbmc_a, cbmc_b, cbmc_c);

  __CPROVER_assert(
      (cbmc_a > cbmc_b && cbmc_b > cbmc_c && r == cbmc_b)
      ||
      (cbmc_a > cbmc_b && cbmc_b <= cbmc_c && cbmc_a > cbmc_c && r == cbmc_c)
      ||
      (cbmc_a > cbmc_b && cbmc_b <= cbmc_c && cbmc_a <= cbmc_c && r == cbmc_a)
      ||
      (cbmc_a <= cbmc_b && cbmc_b > cbmc_c && cbmc_a > cbmc_c && r == cbmc_a)
      ||
      (cbmc_a <= cbmc_b && cbmc_b > cbmc_c && cbmc_a <= cbmc_c && r == cbmc_c)
      ||
      (cbmc_a <= cbmc_b && cbmc_b <= cbmc_c && r == cbmc_b)
      ,"FAIL");
}
int main_inner(int cbmc_a, int cbmc_b, int cbmc_c) 
{ int a ;
  int b ;
  int c ;
  int median ;

  {
  //printf((char const   */* __restrict  */)"Please enter 3 numbers separated by spaces > ");
  a = cbmc_a; b = cbmc_b; c = cbmc_c;//scanf((char const   */* __restrict  */)"%d%d%d", & a, & b, & c);
  if (b >= a) {
    if (a >= c) {
      return a;//printf((char const   */* __restrict  */)"%d is the median\n", a);
    } else {
      goto _L___3;
    }
  } else {
    _L___3: /* CIL Label */ 
    if (c <= a) {
      if (a <= b) {
        return a;//printf((char const   */* __restrict  */)"%d is the median\n", a);
      } else {
        goto _L___2;
      }
    } else {
      _L___2: /* CIL Label */ 
      if (a >= b) {
        if (b >= c) {
          return b;//printf((char const   */* __restrict  */)"%d is the median\n", b);
        } else {
          goto _L___1;
        }
      } else {
        _L___1: /* CIL Label */ 
        if (a <= b) {
          if (b <= c) {
            return b;//printf((char const   */* __restrict  */)"%d is the median\n", b);
          } else {
            goto _L___0;
          }
        } else {
          _L___0: /* CIL Label */ 
          if (a >= c) {
            if (c >= b) {
              return c;//printf((char const   */* __restrict  */)"%d is the median\n", c);
            } else {
              goto _L;
            }
          } else {
            _L: /* CIL Label */ 
            if (a <= c) {
              if (c <= b) {
                return c;//printf((char const   */* __restrict  */)"%d is the median\n", c);
              } else {
                __repair_rep_1__6: /* CIL Label */ 
                {
                return a;//printf((char const   */* __restrict  */)"%d is the median\n", a);
                }
              }
            } else {
              return (1);
            }
          }
        }
      }
    }
  }
  return (0);
}
}
