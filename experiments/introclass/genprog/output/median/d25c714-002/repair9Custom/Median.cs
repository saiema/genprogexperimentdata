﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class median{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int MedianImplementation(int pex_a, int pex_b, int pex_c, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int complete_path){

      Contract.Ensures(
          (pex_a > pex_b && pex_b > pex_c && Contract.Result<int>() == pex_b)
          ||
          (pex_a > pex_b && pex_b <= pex_c && pex_a > pex_c && Contract.Result<int>() == pex_c)
          ||
          (pex_a > pex_b && pex_b <= pex_c && pex_a <= pex_c && Contract.Result<int>() == pex_a)
          ||
          (pex_a <= pex_b && pex_b > pex_c && pex_a > pex_c && Contract.Result<int>() == pex_a)
          ||
          (pex_a <= pex_b && pex_b > pex_c && pex_a <= pex_c && Contract.Result<int>() == pex_c)
          ||
          (pex_a <= pex_b && pex_b <= pex_c && Contract.Result<int>() == pex_b)
          );
      
      output = "";
      
      pex_input = 0;

      //begin solution
  int A = init_int_1;
  int B = init_int_2;
  int C = init_int_3;
  int Small = init_int_4;
  int Large = init_int_5;
  int Median = init_int_1;

  {
    //printf((char const * /* __restrict  */) "Please enter 3 numbers separated by spaces > ");
   //scanf((char const * /* __restrict  */) "%d%d%d", &A, &B, &C);
A = pex_a; B = pex_b; C = pex_c;
    if (A == B) {
    __repair_rep_7__2 : /* CIL Label */
    {
      if (A > B) {
        Large = A;
        Small = B;
      } else {
        Large = B;
        Small = A;
      }
    }
    } else {
    _L: /* CIL Label */
      if (A > B) {
        Large = A;
        Small = B;
      } else {
        Large = B;
        Small = A;
      }
    }
    if (C > Large) {
      Median = Large;
    } else {
      if (C < Small) {
        Median = Small;
      } else {
        Median = C;
      }
    }
   //printf((char const * /* __restrict  */) "%d is the median\n", Median);
return (int) (Median);
    return (int) (0);
  }
return (int) complete_path;
}}}
