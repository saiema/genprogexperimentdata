int nondet_int();

int main_inner(int cbmc_a, int cbmc_b, int cbmc_c);
int main(){

  int cbmc_a = nondet_int();
  int cbmc_b = nondet_int();
  int cbmc_c = nondet_int();

  int r = main_inner(cbmc_a, cbmc_b, cbmc_c);

  __CPROVER_assert(
      (cbmc_a > cbmc_b && cbmc_b > cbmc_c && r == cbmc_b)
      ||
      (cbmc_a > cbmc_b && cbmc_b <= cbmc_c && cbmc_a > cbmc_c && r == cbmc_c)
      ||
      (cbmc_a > cbmc_b && cbmc_b <= cbmc_c && cbmc_a <= cbmc_c && r == cbmc_a)
      ||
      (cbmc_a <= cbmc_b && cbmc_b > cbmc_c && cbmc_a > cbmc_c && r == cbmc_a)
      ||
      (cbmc_a <= cbmc_b && cbmc_b > cbmc_c && cbmc_a <= cbmc_c && r == cbmc_c)
      ||
      (cbmc_a <= cbmc_b && cbmc_b <= cbmc_c && r == cbmc_b)
      ,"FAIL");
}
int main_inner(int cbmc_a, int cbmc_b, int cbmc_c) 
{ int A ;
  int B ;
  int C ;
  int Small ;
  int Large ;
  int Median ;

  {
  //printf((char const   */* __restrict  */)"Please enter 3 numbers separated by spaces > ");
  A = cbmc_a; B = cbmc_b; C = cbmc_c;//scanf((char const   */* __restrict  */)"%d%d%d", & A, & B, & C);
  __repair_app_7__d: /* CIL Label */ 
  {
  if (A == B) {
    if (A == C) {
      Median = A;
    } else {
      goto _L;
    }
  } else {
    _L: /* CIL Label */ 
    if (A > B) {
      Large = A;
      Small = B;
    } else {
      Large = B;
      Small = A;
    }
  }
  if (A > B) {
    Large = A;
    Small = B;
  } else {
    Large = B;
    Small = A;
  }
  }
  if (C > Large) {
    Median = Large;
  } else {
    if (C < Small) {
      Median = Small;
    } else {
      Median = C;
    }
  }
  return Median;//printf((char const   */* __restrict  */)"%d is the median\n", Median);
  return (0);
}
}
