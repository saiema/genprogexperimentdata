﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class median{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int MedianImplementation(int pex_a, int pex_b, int pex_c, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int complete_path){

      Contract.Ensures(
          (pex_a > pex_b && pex_b > pex_c && Contract.Result<int>() == pex_b)
          ||
          (pex_a > pex_b && pex_b <= pex_c && pex_a > pex_c && Contract.Result<int>() == pex_c)
          ||
          (pex_a > pex_b && pex_b <= pex_c && pex_a <= pex_c && Contract.Result<int>() == pex_a)
          ||
          (pex_a <= pex_b && pex_b > pex_c && pex_a > pex_c && Contract.Result<int>() == pex_a)
          ||
          (pex_a <= pex_b && pex_b > pex_c && pex_a <= pex_c && Contract.Result<int>() == pex_c)
          ||
          (pex_a <= pex_b && pex_b <= pex_c && Contract.Result<int>() == pex_b)
          );
      
      output = "";
      
      pex_input = 0;

      //begin solution
  int a = init_int_1;
  int b = init_int_2;
  int c = init_int_3;
  int median = init_int_4;
  int temp = init_int_5;

  {
  __repair_app_4__23f : /* CIL Label */
  {
    median = 0;
  }
    temp = 0;
    //printf((char const * /* __restrict  */) "Please enter 3 numbers separated by spaces > ");
   //scanf((char const * /* __restrict  */) "%d%d%d", &a, &b, &c);
a = pex_a; b = pex_b; c = pex_c;
    if (a >= b) {
      temp = b;
      b = a;
      a = temp;
    } else {
    }
  __repair_app_4__240 : /* CIL Label */
  {
    if (c > b) {
      median = b;
    } else {
    __repair_rep_8__241 : /* CIL Label */
    {
      if (c > a) {
        median = c;
      } else {
        median = a;
      }
    }
    }
  }
   //printf((char const * /* __restrict  */) "%d is the median\n", median);
return (int) (median);
  __repair_rep_15__242 : /* CIL Label */
  {
    if (a >= b) {
      temp = b;
      b = a;
      a = temp;
    } else {
    }
  }
  }
return (int) complete_path;
}}}
