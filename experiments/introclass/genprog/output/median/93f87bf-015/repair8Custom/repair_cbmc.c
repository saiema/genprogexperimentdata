int nondet_int();

int main_inner(int cbmc_a, int cbmc_b, int cbmc_c);
int main(){

  int cbmc_a = nondet_int();
  int cbmc_b = nondet_int();
  int cbmc_c = nondet_int();

  int r = main_inner(cbmc_a, cbmc_b, cbmc_c);

  __CPROVER_assert(
      (cbmc_a > cbmc_b && cbmc_b > cbmc_c && r == cbmc_b)
      ||
      (cbmc_a > cbmc_b && cbmc_b <= cbmc_c && cbmc_a > cbmc_c && r == cbmc_c)
      ||
      (cbmc_a > cbmc_b && cbmc_b <= cbmc_c && cbmc_a <= cbmc_c && r == cbmc_a)
      ||
      (cbmc_a <= cbmc_b && cbmc_b > cbmc_c && cbmc_a > cbmc_c && r == cbmc_a)
      ||
      (cbmc_a <= cbmc_b && cbmc_b > cbmc_c && cbmc_a <= cbmc_c && r == cbmc_c)
      ||
      (cbmc_a <= cbmc_b && cbmc_b <= cbmc_c && r == cbmc_b)
      ,"FAIL");
}
int main_inner(int cbmc_a, int cbmc_b, int cbmc_c) 
{ int int1 ;
  int int2 ;
  int int3 ;

  {
  //printf((char const   */* __restrict  */)"Please enter 3 numbers separated by spaces > ");
  __repair_app_14__f6: /* CIL Label */ 
  {
  int1 = cbmc_a; int2 = cbmc_b; int3 = cbmc_c;//scanf((char const   */* __restrict  */)"%d %d %d", & int1, & int2, & int3);

  }
  if (int1 <= int2) {
    if (int1 >= int3) {
      return int1;//printf((char const   */* __restrict  */)"%d is the median \n", int1);
    } else {
      goto _L___3;
    }
  } else {
    _L___3: /* CIL Label */ 
    if (int1 <= int2) {
      if (int1 >= int3) {
        return int1;//printf((char const   */* __restrict  */)"%d is the median \n", int1);
      } else {
        goto _L___2;
      }
    } else {
      _L___2: /* CIL Label */ 
      if (int2 <= int1) {
        if (int2 >= int3) {
          return int2;//printf((char const   */* __restrict  */)"%d is the median \n", int2);
        } else {
          goto _L___1;
        }
      } else {
        _L___1: /* CIL Label */ 
        if (int2 <= int3) {
          if (int2 >= int1) {
            return int2;//printf((char const   */* __restrict  */)"%d is the median \n", int2);
          } else {
            goto _L___0;
          }
        } else {
          _L___0: /* CIL Label */ 
          if (int3 <= int1) {
            if (int3 >= int2) {
              return int3;//printf((char const   */* __restrict  */)"%d is the median \n", int3);
            } else {
              goto _L;
            }
          } else {
            _L: /* CIL Label */ 
            if (int3 <= int2) {
              if (int3 >= int1) {
                return int3;//printf((char const   */* __restrict  */)"%d is the median \n", int3);
              } else {

              }
            } else {
              __repair_app_1__f7: /* CIL Label */ 
              {

              return int1;//printf((char const   */* __restrict  */)"%d is the median \n", int1);
              }
            }
          }
        }
      }
    }
  }
  __repair_del_23__f8: /* CIL Label */ 
  {

  }
}
}
