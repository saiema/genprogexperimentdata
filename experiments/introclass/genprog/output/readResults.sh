#syllables/b1924d6-005/experimentsCustom.txt

caseStudy=$1

find $caseStudy -name "experimentsCustom.txt" | while read line; do
 
  folder=$(echo $line | cut -d'/' -f -2 )
  variant=$(echo $line | cut -d'/' -f 2 )

  repairs=$(find $folder -name "repair*Custom" | wc -l)
  candidates=$(find $folder -name "candidate" | wc -l)
  spurious=$(find $folder -name "spurious" | wc -l)
  fixes=$(find $folder -name "fix_*" | wc -l)
  to=$(grep "TO" $line | wc -l)

  echo "$caseStudy	$variant	$repairs	$fixes	$candidates	$spurious	$to"
 
done
