﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class smallest{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;
    
    public static int pex_input;

    public static int SmallestImplementation(int pex_a, int pex_b, int pex_c, int pex_d, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2,int complete_path){

      Contract.Ensures(Contract.Result<int>() == pex_a || Contract.Result<int>() == pex_b || Contract.Result<int>() == pex_c || Contract.Result<int>() == pex_d); 
      Contract.Ensures(Contract.Result<int>() <= pex_a && Contract.Result<int>() <= pex_b && Contract.Result<int>() <= pex_c && Contract.Result<int>() <= pex_d);

      output = "";

      pex_input = 0;
      //begin solution
  int n1 = init_int_1;
  int n2 = init_int_2;
  int n3 = init_int_3;
  int n4 = init_int_4;
  int min = init_int_5;

  {
    //printf((char const * /* __restrict  */) "Please enter 4 numbers separated by spaces > ");
   //scanf((char const * /* __restrict  */) "%d%d%d%d", &n1, &n2, &n3, &n4);
n1 = pex_a; n2 = pex_b; n3 = pex_c; n4 = pex_d;
    if (n1 <= n2) {
      min = n1;
    } else {
      min = n2;
    }
    if (min >= n3) {
      min = n3;
    } else {
    }
    if (min >= n4) {
    __repair_app_6__17 : /* CIL Label */
    {
      min = n4;
     //printf((char const * /* __restrict  */) "%d is the smallest\n", min);
return (min);
    }
    } else {
     //printf((char const * /* __restrict  */) "%d is the smallest\n", min);
return (min);
    }
    return (0);
  }
return complete_path;
}}}
