﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class smallest{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;
    
    public static int pex_input;

    public static int SmallestImplementation(int pex_a, int pex_b, int pex_c, int pex_d, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2,int complete_path){

      Contract.Ensures(Contract.Result<int>() == pex_a || Contract.Result<int>() == pex_b || Contract.Result<int>() == pex_c || Contract.Result<int>() == pex_d); 
      Contract.Ensures(Contract.Result<int>() <= pex_a && Contract.Result<int>() <= pex_b && Contract.Result<int>() <= pex_c && Contract.Result<int>() <= pex_d);

      output = "";

      pex_input = 0;
      //begin solution
  int first = init_int_1;
  int second = init_int_2;
  int third = init_int_3;
  int fourth = init_int_4;
  int lowest = init_int_5;

  {
    //printf((char const * /* __restrict  */) "Please enter 4 numbers separated by spaces > ");
  __repair_app_2__158 : /* CIL Label */
  {
   //scanf((char const * /* __restrict  */) "%d%d%d%d", &first, &second, &third, &fourth);
first = pex_a; second = pex_b; third = pex_c; fourth = pex_d;
    lowest = second;
  }
    if (first < second) {
      lowest = first;
    } else {
      if (second < first) {
        lowest = second;
      } else {
      __repair_rep_10__159 : /* CIL Label */
      {
       //scanf((char const * /* __restrict  */) "%d%d%d%d", &first, &second, &third, &fourth);
      }
      }
    }
    if (third < lowest) {
      lowest = third;
    } else {
    }
    if (fourth < lowest) {
    __repair_app_12__15a : /* CIL Label */
    {
      lowest = fourth;
      if (third < lowest) {
        lowest = third;
      } else {
      }
    }
    } else {
    }
   //printf((char const * /* __restrict  */) "%d is the smallest\n", lowest);
return (lowest);
  __repair_del_15__15b : /* CIL Label */
  {}
  }
return complete_path;
}}}
