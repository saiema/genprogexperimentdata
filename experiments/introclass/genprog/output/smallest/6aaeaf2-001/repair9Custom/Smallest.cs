﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class smallest{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;
    
    public static int pex_input;

    public static int SmallestImplementation(int pex_a, int pex_b, int pex_c, int pex_d, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2,int complete_path){

      Contract.Ensures(Contract.Result<int>() == pex_a || Contract.Result<int>() == pex_b || Contract.Result<int>() == pex_c || Contract.Result<int>() == pex_d); 
      Contract.Ensures(Contract.Result<int>() <= pex_a && Contract.Result<int>() <= pex_b && Contract.Result<int>() <= pex_c && Contract.Result<int>() <= pex_d);

      output = "";

      pex_input = 0;
      //begin solution
  int int1 = init_int_1;
  int int2 = init_int_2;
  int int3 = init_int_3;
  int int4 = init_int_4;
  int tmp = init_int_5;

  {
    //printf((char const * /* __restrict  */) "Please enter 4 numbers separated by spaces > ");
   //scanf((char const * /* __restrict  */) "%i%i%i%i", &int1, &int2, &int3, &int4);
int1 = pex_a; int2 = pex_b; int3 = pex_c; int4 = pex_d;
    tmp = int1;
  __repair_app_6__93 : /* CIL Label */
  {
    if (int1 > int2) {
    __repair_app_5__94 : /* CIL Label */
    {
      tmp = int2;
      if (tmp > int4) {
        tmp = int4;
      } else {
      }
    }
    } else {
      if (tmp > int3) {
        tmp = int3;
      } else {
        if (tmp > int4) {
          tmp = int4;
        } else {
        }
      }
    }
    if (tmp > int3) {
      tmp = int3;
    } else {
      if (tmp > int4) {
        tmp = int4;
      } else {
      }
    }
  }
   //printf((char const * /* __restrict  */) "%i is the smallest\n", tmp);
return (tmp);
    return (0);
  }
return complete_path;
}}}
