﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class syllables{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int SyllablesImplementation(string pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int complete_path){

      Contract.Requires(pex_a != null);
      Contract.Requires(1 <= pex_a.Length && pex_a.Length <= 20);
      Contract.Requires(pex_a.Count(x => (x == 'A' || x == 'E' || x == 'I' || x == 'O' || x == 'U' || x == 'Y')) == 0);

      Contract.Ensures(pex_a.Count(x => (x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u' || x == 'y')) == Contract.Result<int>());

      output = "";

      pex_input = 0;

      //begin solution
  //char input[20];
char[] input = new char[20]; input = pex_a.ToCharArray();
  int size = init_int_1;
  int num_vows = init_int_2;
  int i = init_int_3;
  int tmp = init_int_4;

  {
    //printf((char const * /* __restrict  */) "Please enter a stringreserved > ");
    //fgets((char * /* __restrict  */)(input), 19, (FILE * /* __restrict  */) stdin);
    tmp = //strlen((char const *)(input));
input.Length;
    size = (int)(tmp - 1);
    num_vows = 0;
    i = 0;
  __repair_app_23__12e : /* CIL Label */
  {
    while (i < size) {
      if ((int)input[i] == 97) {
        num_vows++;
      } else {
        if ((int)input[i] == 65) {
          num_vows++;
        } else {
          if ((int)input[i] == 101) {
            num_vows++;
          } else {
            if ((int)input[i] == 69) {
              num_vows++;
            } else {
              if ((int)input[i] == 105) {
                num_vows++;
              } else {
                if ((int)input[i] == 73) {
                  num_vows++;
                } else {
                  if ((int)input[i] == 111) {
                    num_vows++;
                  } else {
                    if ((int)input[i] == 79) {
                      num_vows++;
                    } else {
                      if ((int)input[i] == 117) {
                        num_vows++;
                      } else {
                        if ((int)input[i] == 85) {
                          num_vows++;
                        } else {
                          if ((int)input[i] == 121) {
                            num_vows++;
                          } else {
                            if ((int)input[i] == 89) {
                              num_vows++;
                            } else {
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      i++;
    }
    if ((int)input[i] == 69) {
      num_vows++;
    } else {
      if ((int)input[i] == 105) {
        num_vows++;
      } else {
        if ((int)input[i] == 73) {
          num_vows++;
        } else {
          if ((int)input[i] == 111) {
            num_vows++;
          } else {
            if ((int)input[i] == 79) {
              num_vows++;
            } else {
              if ((int)input[i] == 117) {
                num_vows++;
              } else {
                if ((int)input[i] == 85) {
                  num_vows++;
                } else {
                  if ((int)input[i] == 121) {
                    num_vows++;
                  } else {
                    if ((int)input[i] == 89) {
                      num_vows++;
                    } else {
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
   //printf((char const * /* __restrict  */) "The number of syllables is %d.\n", num_vows);
return (num_vows);
  __repair_app_18__12f : /* CIL Label */
  {
    return (0);
    if ((int)input[i] == 117) {
      num_vows++;
    } else {
      if ((int)input[i] == 85) {
        num_vows++;
      } else {
        if ((int)input[i] == 121) {
          num_vows++;
        } else {
          if ((int)input[i] == 89) {
            num_vows++;
          } else {
          }
        }
      }
    }
  }
  }
return complete_path;
}}}
