﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class digits{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int pex_output;

    public static int sign;

    // Only for digits/f5b56c7-007/repair4Custom/repair.c 
    // Genprog modified this method
    public static int check2(int a, int b) {
        if (a >= b) {
          return (a);
        } else {
          return (a);
        }
    }

    public static int[] DigitsImplementation(int pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int[] complete_path){

      //Math.Abs fails
      Contract.Requires(pex_a != int.MinValue);

      Contract.Ensures(Math.Abs(pex_a).ToString().Length == pex_output);

      Contract.Ensures(
          (pex_a < 0 && Contract.ForAll(0, pex_a.ToString().Length - 2, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48)
           && (pex_a.ToString()[1] - 48) == Contract.Result<int[]>()[pex_a.ToString().Length - 2] * -1)
          ||
          (pex_a >= 0 && Contract.ForAll(0, pex_a.ToString().Length, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48))
          );

      int[] pex_results = new int[50];

      output = "";

      pex_input = 0;

      pex_output = 0;

      sign = 1;

      //begin solution
  int integer = init_int_1;
  int i = init_int_2;
  int digit = init_int_3;
  double tmp = init_int_1;
  double tmp___0 = init_int_2;
  double tmp___1 = init_int_3;

  {
    //printf((char const * /* __restrict  */) "\nEnter an integer > ");
   //scanf((char const * /* __restrict  */) "\n%d", &integer);
integer = pex_a;
  __repair_rep_17__7b3 : /* CIL Label */
  {
    tmp___1 = Math.Abs((double)integer);
  }
    i = 1;
    while (i <= 10) {
      digit = integer % 10;
      if (integer == 0) {
        //printf((char const * /* __restrict  */) "0\n");
        break;
      } else {
      __repair_del_7__7b4 : /* CIL Label */
      {}
      __repair_rep_18__7b5 : /* CIL Label */
      {
        if (tmp___1 < (double)10) {
          if (integer != 0) {
           //printf((char const * /* __restrict  */) "%d\n", integer);
pex_results[pex_output++] = sign*(integer); sign = 1;
            break;
          } else {
          }
        } else {
        }
      }
      }
      integer /= 10;
      tmp___1 = Math.Abs((double)integer);
    __repair_rep_15__7b6 : /* CIL Label */
    {
      if (integer == 0) {
        //printf((char const * /* __restrict  */) "0\n");
        break;
      } else {
        tmp___0 = Math.Abs((double)digit);
        if (tmp___0 < (double)10) {
          tmp = Math.Abs((double)digit);
          digit = (int)tmp;
         //printf((char const * /* __restrict  */) "%d\n", digit);
pex_results[pex_output++] = sign*(digit); sign = 1;
        } else {
        }
      }
    }
      i++;
    }
    //printf((char const * /* __restrict  */) "\nThat\'s all, have a nice day!\n");
  __repair_del_26__7b7 : /* CIL Label */
  {}
  }
return pex_results;
}}}
