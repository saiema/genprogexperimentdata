﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class digits{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int pex_output;

    public static int sign;

    // Only for digits/f5b56c7-007/repair4Custom/repair.c 
    // Genprog modified this method
    public static int check2(int a, int b) {
        if (a >= b) {
          return (a);
        } else {
          return (a);
        }
    }

    public static int[] DigitsImplementation(int pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int[] complete_path){

      //Math.Abs fails
      Contract.Requires(pex_a != int.MinValue);

      Contract.Ensures(Math.Abs(pex_a).ToString().Length == pex_output);

      Contract.Ensures(
          (pex_a < 0 && Contract.ForAll(0, pex_a.ToString().Length - 2, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48)
           && (pex_a.ToString()[1] - 48) == Contract.Result<int[]>()[pex_a.ToString().Length - 2] * -1)
          ||
          (pex_a >= 0 && Contract.ForAll(0, pex_a.ToString().Length, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48))
          );

      int[] pex_results = new int[50];

      output = "";

      pex_input = 0;

      pex_output = 0;

      sign = 1;

      //begin solution
  int n = init_int_1;
  int[] d = new int[9];
  int negative = init_int_2;
  int size = init_int_3;
  int tmp = init_int_4;
  double tmp___0 = init_int_1;
  int i = init_int_5;
  int remainder___0 = init_int_1;
  double tmp___1 = init_int_2;
  int multiplier = init_int_2;
  double tmp___2 = init_int_3;

  {
    negative = 0;
    //printf((char const * /* __restrict  */) "Enter an integer > ");
  __repair_rep_8__44 : /* CIL Label */
  {}
    size = 0;
     while (true) {
      tmp = Math.Abs(n);
      tmp___0 = Math.Pow((double)10, (double)size);
    __repair_app_32__45 : /* CIL Label */
    {
      if ((double)tmp >= tmp___0) {
      } else {
        break;
      }
     //scanf((char const * /* __restrict  */) "%d", &n);
     n = pex_a;
    }
      size++;
    }
    if (n < 0) {
      negative = 1;
    } else {
    }
    i = 0;
    while (i < 10) {
      tmp___1 = Math.Pow((double)10, (double)(i + 1));
      remainder___0 = n % (int)tmp___1;
      tmp___2 = Math.Pow((double)10, (double)i);
      multiplier = (int)tmp___2;
      d[i] = remainder___0 / multiplier;
      n -= remainder___0;
      i++;
    }
    //if (negative)
if (negative != 0) {
      i = 0;
      while (i < size - 1) {
       //printf((char const * /* __restrict  */) "%d\n", d[i] * -1);
pex_results[pex_output++] = sign*(d[i] * -1); sign = 1;
        i++;
      }
     //printf((char const * /* __restrict  */) "%d\n", d[size - 1]);
pex_results[pex_output++] = sign*(d[size - 1]); sign = 1;
    } else {
      i = 0;
      while (i < size) {
       //printf((char const * /* __restrict  */) "%d\n", d[i]);
pex_results[pex_output++] = sign*(d[i]); sign = 1;
        i++;
      }
    }
    //printf((char const * /* __restrict  */) "That\'s all, have a nice day!\n");
    return pex_results;
  }
return pex_results;
}}}
