﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class digits{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int pex_output;

    public static int sign;

    // Only for digits/f5b56c7-007/repair4Custom/repair.c 
    // Genprog modified this method
    public static int check2(int a, int b) {
        if (a >= b) {
          return (a);
        } else {
          return (a);
        }
    }

    public static int[] DigitsImplementation(int pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int[] complete_path){

      //Math.Abs fails
      Contract.Requires(pex_a != int.MinValue);

      Contract.Ensures(Math.Abs(pex_a).ToString().Length == pex_output);

      Contract.Ensures(
          (pex_a < 0 && Contract.ForAll(0, pex_a.ToString().Length - 2, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48)
           && (pex_a.ToString()[1] - 48) == Contract.Result<int[]>()[pex_a.ToString().Length - 2] * -1)
          ||
          (pex_a >= 0 && Contract.ForAll(0, pex_a.ToString().Length, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48))
          );

      int[] pex_results = new int[50];

      output = "";

      pex_input = 0;

      pex_output = 0;

      sign = 1;

      //begin solution
  int n = init_int_1;
  int r = init_int_2;
  int nn = init_int_3;

  {
    //printf((char const * /* __restrict  */) "\nEnter an integer > ");
  __repair_app_19__4d3 : /* CIL Label */
  {
   //scanf((char const * /* __restrict  */) "%d", &n);
n = pex_a;
    if (n < 0) {
      n = -n;
      //while (n)
while (n != 0) {
        r = n % 10;
        nn = n / 10;
        if (n < 10) {
         //printf((char const * /* __restrict  */) "\n-%d", r);
pex_results[pex_output++] = sign*(-(r)); sign = 1;
        } else {
          if (n >= 10) {
           //printf((char const * /* __restrict  */) "\n%d", r);
pex_results[pex_output++] = sign*(r); sign = 1;
          } else {
          }
        }
        n = nn;
      }
    } else {
      r = n % 10;
      nn = n / 10;
      n = nn;
     //printf((char const * /* __restrict  */) "\n%d", r);
pex_results[pex_output++] = sign*(r); sign = 1;
    }
  }
    //while (n)
while (n != 0) {
      if (n < 0) {
        n = -n;
        //while (n)
while (n != 0) {
          r = n % 10;
          nn = n / 10;
          if (n < 10) {
           //printf((char const * /* __restrict  */) "\n-%d", r);
pex_results[pex_output++] = sign*(-(r)); sign = 1;
          } else {
            if (n >= 10) {
             //printf((char const * /* __restrict  */) "\n%d", r);
pex_results[pex_output++] = sign*(r); sign = 1;
            } else {
            }
          }
          n = nn;
        }
      } else {
        r = n % 10;
        nn = n / 10;
        n = nn;
       //printf((char const * /* __restrict  */) "\n%d", r);
pex_results[pex_output++] = sign*(r); sign = 1;
      }
    }
  __repair_app_21__4d4 : /* CIL Label */
  {
    //printf((char const * /* __restrict  */) "\nThat\'s all, have a nice day!\n");
   //scanf((char const * /* __restrict  */) "%d", &n);
  }
    return pex_results;
  }
return pex_results;
}}}
