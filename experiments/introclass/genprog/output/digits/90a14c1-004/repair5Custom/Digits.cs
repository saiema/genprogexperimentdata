﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class digits{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int pex_output;

    public static int sign;

    // Only for digits/f5b56c7-007/repair4Custom/repair.c 
    // Genprog modified this method
    public static int check2(int a, int b) {
        if (a >= b) {
          return (a);
        } else {
          return (a);
        }
    }

    public static int[] DigitsImplementation(int pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int[] complete_path){

      //Math.Abs fails
      Contract.Requires(pex_a != int.MinValue);

      Contract.Ensures(Math.Abs(pex_a).ToString().Length == pex_output);

      Contract.Ensures(
          (pex_a < 0 && Contract.ForAll(0, pex_a.ToString().Length - 2, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48)
           && (pex_a.ToString()[1] - 48) == Contract.Result<int[]>()[pex_a.ToString().Length - 2] * -1)
          ||
          (pex_a >= 0 && Contract.ForAll(0, pex_a.ToString().Length, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48))
          );

      int[] pex_results = new int[50];

      output = "";

      pex_input = 0;

      pex_output = 0;

      sign = 1;

      //begin solution
  int valuereserved = init_int_1;
  int digit = init_int_2;

  {
    //printf((char const * /* __restrict  */) "\nEnter an integer > ");
  __repair_app_21__1e9 : /* CIL Label */
  {
   //scanf((char const * /* __restrict  */) "%d", &valuereserved);
valuereserved = pex_a;
    if (valuereserved > 0) {
      while (valuereserved != 0) {
        digit = valuereserved % 10;
        valuereserved /= 10;
       //printf((char const * /* __restrict  */) "%d\n", digit);
pex_results[pex_output++] = sign*(digit); sign = 1;
      }
    } else {
      if (valuereserved < 0) {
        while (valuereserved < -10) {
          digit = Math.Abs(valuereserved % 10);
          valuereserved /= 10;
         //printf((char const * /* __restrict  */) "%d\n", digit);
pex_results[pex_output++] = sign*(digit); sign = 1;
        }
      } else {
        if (valuereserved == 0) {
          digit = 0;
         //printf((char const * /* __restrict  */) "%d\n", digit);
pex_results[pex_output++] = sign*(digit); sign = 1;
        } else {
        }
      }
    }
  }
    //printf((char const * /* __restrict  */) "\n");
    if (valuereserved > 0) {
      while (valuereserved != 0) {
        digit = valuereserved % 10;
        valuereserved /= 10;
       //printf((char const * /* __restrict  */) "%d\n", digit);
pex_results[pex_output++] = sign*(digit); sign = 1;
      }
    } else {
    __repair_rep_6__1ea : /* CIL Label */
    {
      while (valuereserved != 0) {
        digit = valuereserved % 10;
        valuereserved /= 10;
       //printf((char const * /* __restrict  */) "%d\n", digit);
pex_results[pex_output++] = sign*(digit); sign = 1;
      }
    }
    }
    //printf((char const * /* __restrict  */) "That\'s all, have a nice day!\n");
    return pex_results;
  }
return pex_results;
}}}
