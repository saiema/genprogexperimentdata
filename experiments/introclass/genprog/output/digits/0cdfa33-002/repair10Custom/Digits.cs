﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class digits{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process  
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;

    public static int pex_output;

    public static int sign;

    // Only for digits/f5b56c7-007/repair4Custom/repair.c 
    // Genprog modified this method
    public static int check2(int a, int b) {
        if (a >= b) {
          return (a);
        } else {
          return (a);
        }
    }

    public static int[] DigitsImplementation(int pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, int[] complete_path){

      //Math.Abs fails
      Contract.Requires(pex_a != int.MinValue);

      Contract.Ensures(Math.Abs(pex_a).ToString().Length == pex_output);

      Contract.Ensures(
          (pex_a < 0 && Contract.ForAll(0, pex_a.ToString().Length - 2, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48)
           && (pex_a.ToString()[1] - 48) == Contract.Result<int[]>()[pex_a.ToString().Length - 2] * -1)
          ||
          (pex_a >= 0 && Contract.ForAll(0, pex_a.ToString().Length, pex_i => Contract.Result<int[]>()[pex_i] == pex_a.ToString()[pex_a.ToString().Length - 1 - pex_i] - 48))
          );

      int[] pex_results = new int[50];

      output = "";

      pex_input = 0;

      pex_output = 0;

      sign = 1;

      //begin solution
  int c = init_int_1;
  int i = init_int_2;
  int flag1 = init_int_3;
  int flag2 = init_int_4;
  //char digit[11];
char[] digit = new char[11]; digit = pex_a.ToString().ToCharArray();
  int tmp = init_int_5;

  {
    flag2 = 0;
    //printf((char const * /* __restrict  */) "\nEnter an integer > ");
   //scanf((char const * /* __restrict  */) "%s", &digit);
    tmp = //strlen((char const *)(digit));
digit.Length;
    c = (int)tmp;
    //printf((char const * /* __restrict  */) "\n");
    i = 1;
    while (i < c) {
      if ((int)digit[i] == 45) {
        flag1 = i;
        break;
      } else {
        flag1 = c;
      }
      i++;
    }
    if ((int)digit[0] == 45) {
      flag2 = 1;
    } else {
    }
  __repair_rep_20__1 : /* CIL Label */
  {
   //scanf((char const * /* __restrict  */) "%s", &digit);
  }
    while (i >= flag2) {
      if (flag2 == 1) {
        if (i == 1) {
          //printf((char const * /* __restrict  */) "-")
sign = -1;
        } else {
        }
      } else {
      }
     //printf((char const * /* __restrict  */) "%c\n", digit[i]);
if(i != digit.Length) {pex_results[pex_output++] = sign*(digit[i] - 48); sign = 1;}
      i--;
    }
    //printf((char const * /* __restrict  */) "That\'s all, have a nice day!\n");
    return pex_results;
  }
return pex_results;
}}}
