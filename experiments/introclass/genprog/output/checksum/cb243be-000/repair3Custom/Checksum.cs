﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.Contracts;
using System.Linq;

namespace introclassCSharp{

  public class checksum{

    [AssemblyInitialize]
    public static void Initialize(TestContext ctx){
      // avoid contract violation kill the process 
      Contract.ContractFailed += new EventHandler<ContractFailedEventArgs>(Contract_ContractFailed);
    }

    static void Contract_ContractFailed(object sender, System.Diagnostics.Contracts.ContractFailedEventArgs e){
      e.SetHandled();
      Assert.Fail("{0}: {1} {2}", e.FailureKind, e.Message, e.Condition);
    }

    public static string output;

    public static int pex_input;
   
    public static char ChecksumImplementation(string pex_a, int init_int_1, int init_int_2, int init_int_3, int init_int_4, int init_int_5, char init_char_1, char init_char_2, char complete_path){

      Contract.Requires(pex_a != null);

      Contract.Ensures(((pex_a.Sum(x => x) % 64) + 32) == Contract.Result<char>());
      
      output = "";

      pex_input = 0;
      
      //begin solution
  int num = init_int_1;
  char i = init_char_1;
  char final = init_char_2;

  {
    //printf((char const * /* __restrict  */) "Enter an abitrarily long stringreserved, ending with carriage return > ");
  __repair_del_10__26d : /* CIL Label */
  {}
    num = (int)i;
    while ((int)i != 10) {
      if (num != (int)i) {
        if ((int)i != 10) {
          num += (int)i;
        } else {
        }
      } else {
      __repair_rep_4__26e : /* CIL Label */
      {
        if ((int)i != 10) {
          num += (int)i;
        } else {
        }
      }
      }
      //scanf((char const * /* __restrict  */) "%c", &i);
i = pex_a[pex_input++];
    }
  __repair_app_12__26f : /* CIL Label */
  {
    final = (char)(num % 64);
    while ((int)i != 10) {
      if (num != (int)i) {
        if ((int)i != 10) {
          num += (int)i;
        } else {
        }
      } else {
      }
      //scanf((char const * /* __restrict  */) "%c", &i);
i = pex_a[pex_input++];
    }
  }
    final = (char)((int)final + 32);
   //printf((char const * /* __restrict  */) "Check sum is %c\n", final);
return (final);
    return (char) (0);
  }
return complete_path;
}}}
