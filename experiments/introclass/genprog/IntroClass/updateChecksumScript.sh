#!/bin/bash

#partialExperiments=$(find checksum/ | awk '/experimentsB/' | xargs -I {} wc -l {} | grep "^1 " | sed 's/1 //g' | grep -v "\-PARTIAL")

#linesToUncomment=$(echo partialExperiments | xargs -I {} dirname {} | sed 's/checksum\///g' | xargs -I {} grep "{}" ../scripts/runGenProgExperimentsChecksum.sh | grep "checksumArgsBB" | sed 's/#//g')

linesToUncomment=$(find checksum/ | awk '/experimentsB/' | xargs -I {} wc -l {} | grep "^1 " | sed 's/1 //g' | grep -v "\-PARTIAL" | xargs -I {} dirname {} | sed 's/checksum\///g' | xargs -I {} grep "{}" ../scripts/runGenProgExperimentsChecksum.sh | grep "checksumArgsBB" | sed 's/#//g')

touch ../scripts/runGenProgExperimentsChecksumRestOfRuns.sh

#echo -e $linesToUncomment
#echo $(wc -l $linesToUncomments)

while read line; do
	echo $line
	lineWithoutFirstNumeral=${line/\#}
	#echo "line without first numeral: $lineWithoutFirstNumeral"
	if test "${linesToUncomment#*$lineWithoutFirstNumeral}" != "$linesToUncomment"; then
		#echo "line to uncomment: $line"
		echo $lineWithoutFirstNumeral >> ../scripts/runGenProgExperimentsChecksumRestOfRuns.sh
	else
		#echo "line to write directly: $line"
		echo $line >> ../scripts/runGenProgExperimentsChecksumRestOfRuns.sh
	fi
done < ../scripts/runGenProgExperimentsChecksum.sh

# | xargs -Ip awk -v P=$p '{gsub(#P, P)}' ../scripts/runGenProgExperimentsChecksum.sh
