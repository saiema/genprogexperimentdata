#!/bin/bash

#$1 : program name (e.g.: median)
#$2 : test script (e.g.: test.sh)

program="$1"
script="$2"
outputDirectory="$3"
template="runGenProgExperimentTemplate.sh"

echo "Script : $script"
if [ ! -d "$outputDirectory" ]; then
	mkdir $outputDirectory
fi
for f in $(find . | awk -v pat="$program.c" '$0 ~ pat'); do

	echo "File: "$f
	directory="$(dirname "$f")"
	directory="$(echo $directory | sed 's/\.\///g')"
	echo "Directory: $directory"

	variantID=""	
	isNumberedVariant=0
	mathingNumbers=$(echo ${directory:(-3)} | grep -o "[0-9]")
	if [[ -z  $mathingNumbers ]]; then
		variantID="${directory/\//_}"
	else
		variantID="${directory:0:7}-${directory:(-3)}"	
	fi

	newVariantFile="${template/Template/$variantID}"
	echo "new variant script : $newVariantFile"
	cp $template "$outputDirectory/$newVariantFile"
	pushd $outputDirectory
	
	sed -i "s|<PROGRAM>|$program|g" $newVariantFile
	sed -i "s|<TEST-SCRIPT>|$script|g" $newVariantFile
	sed -i "s|<VARIANT-DIR>|$directory|g" $newVariantFile

	chmod +x $newVariantFile

	popd	

done
