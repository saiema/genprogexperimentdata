#!/bin/bash


pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

pushd "tests"

for f in $(find . | awk '/\.in/'); do

	value=$(cat $f)
	valueNoSpaces=$(echo $value | sed 's/[[:space:]]/\_/g')
	value="$valueNoSpaces"

	cp $f "$f.old"
	rm $f

	size=$(echo $valueNoSpaces | wc -m)


	if [[ "$size" -lt "25" ]]; then
		#echo "$value has size $size, padding"
		padded=$(echo -n "$valueNoSpaces$(printf '\137%.0s' {1..30})" | head -c 21)
		value="$padded"
	fi

	echo "$value" >> $f

done

popd
