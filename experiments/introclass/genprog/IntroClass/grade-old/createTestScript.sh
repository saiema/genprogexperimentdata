#!/bin/bash

#$1 : program name (e.g.: median)
#$2 : test files original postfix (e.g.: -original)

POSITIVE=1
NEGATIVE=0
ORIGINAL=0
EXHAUSTIVE=1

outputFile="test.sh"
program=$1
testFilesOriginalPostFix=$2


function writeInit() {
	local q="\`"
	local outFile=$1
	echo "#!/bin/bash" >> $outFile
	echo "DIR=${q}dirname \$0${q}" >> $outFile
	echo "" >> $outFile
	echo "case \$2 in" >> $outFile
}

function writeEnd() {
	local outFile=$1
	echo "esac" >> $outFile
	echo "exit 1" >> $outFile
}

function writeCases() {
	local program=$1
	local outputFile=$2
	local testFile=$3
	local testIndex=$4
	local count=$5
	local ttype=$6
	local torigin=$7
	local exhaustiveTestIndex=1
	c="ERROR"
	if [[ "$ttype" -eq "$POSITIVE" ]]; then
		c="p"	
	fi
	if [[ "$ttype" -eq "$NEGATIVE" ]]; then
		c="n"	
	fi
	if [[ "$c" == "ERROR" ]]; then
		echo "error detecting test type"
		exit 1	
	fi
	cat $testFile | while read line
	do
		lineToWrite="/$line"
		lineToWriteOut=$(echo $lineToWrite | sed 's/\.in/\.out/g')
		tindex="$c$testIndex)"
		
		scriptPart="ERROR"
		set -f
		if [[ "$torigin" -eq "$ORIGINAL" ]]; then
			scriptPart="./testRepair.sh \$1 $c$exhaustiveTestIndex $ORIGINAL"
			exhaustiveTestIndex=$((exhaustiveTestIndex + 1))
		fi
		if [[ "$torigin" -eq "$EXHAUSTIVE" ]]; then
			scriptPart="./testRepair.sh \$1 $c$exhaustiveTestIndex $EXHAUSTIVE"
			exhaustiveTestIndex=$((exhaustiveTestIndex + 1))
		fi
		if [[ "$torigin" == "ERROR" ]]; then
			echo "error detecting test origin"
			exit 2	
		fi
		
		
		echo "${tindex} ${scriptPart} && exit 0;;" >> $outputFile
		set +f
		testIndex=$((testIndex + 1))
	done
}



for f in $(find . | awk -v pat="$program.c" '$0 ~ pat'); do

	echo "File: "$f
	directory="$(dirname "$f")"
	echo "Directory: "$directory
	pushd $directory	
	
	posTestoriginal="posTests$testFilesOriginalPostFix.txt"
	negTestoriginal="negTests$testFilesOriginalPostFix.txt"
	posTestexhaustive="posTests-exhaustive.txt"
	negTestexhaustive="negTests-exhaustive.txt"
	
	numPTO=0
	if [ -f $posTestoriginal ]; then
		numPTO=$(wc -l < $posTestoriginal)		
	fi
	numNTO=0
	if [ -f $negTestoriginal ]; then
		numNTO=$(wc -l < $negTestoriginal)		
	fi
	numPTE=0
	if [ -f $posTestexhaustive ]; then
		numPTE=$(wc -l < $posTestexhaustive)		
	fi
	numNTE=0
	if [ -f $negTestexhaustive ]; then
		numNTE=$(wc -l < $negTestexhaustive)		
	fi
	
	echo "for file $f there are :"
	echo "$numPTO original positive tests"
	echo "$numNTO original negative tests"
	echo "$numPTE exhaustive positive tests"
	echo "$numNTE exhaustive negative tests"

	touch $outputFile
	initialIndex=1
	writeInit $outputFile
	if [[ "$numPTO" -gt "0" ]]; then
		writeCases $program $outputFile $posTestoriginal $initialIndex $numPTO $POSITIVE $ORIGINAL
		initialIndex=$((initialIndex + $numPTO))
	fi
	if [[ "$numPTE" -gt "0" ]]; then
		writeCases $program $outputFile $posTestexhaustive $initialIndex $numPTE $POSITIVE $EXHAUSTIVE
		initialIndex=$((initialIndex + $numPTE))
	fi
	initialIndex=1
	if [[ "$numNTO" -gt "0" ]]; then
		writeCases $program $outputFile $negTestoriginal $initialIndex $numNTO $NEGATIVE $ORIGINAL
		initialIndex=$((numNTO + 1))
	fi
	if [[ "$numNTE" -gt "0" ]]; then
		writeCases $program $outputFile $negTestexhaustive $initialIndex $numNTE $NEGATIVE $EXHAUSTIVE
	fi	
	writeEnd $outputFile
	chmod +x $outputFile
	popd

done
