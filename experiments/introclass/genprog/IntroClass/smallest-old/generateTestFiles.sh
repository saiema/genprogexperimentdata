#!/bin/bash

#Generates posTests.txt and negTests.txt for introClass GP benchmark


bbtestFolder="tests/blackbox/"
wbtestFolder="tests/whitebox/"
exhaustiveFolder="tests/exhaustive/"
program=$1
whiteTests=$2
blackTests=$3
testFilesPostfix=$4
exhaustiveTests=$5

for f in $(find . | awk -v pat="$program.c" '$0 ~ pat'); do

	echo "File: "$f

	directory="$(dirname "$f")"
	
	echo "Directory: "$directory

	exe="$(basename "$f" .c)"

	echo "Exe: "$exe

	pushd $directory

	gcc -o "$program" "$program.c" -lm -fno-stack-protector
	
	popd

	if [ "$exhaustiveTests" -gt "0" ]; then
		for (( i=1; i<=$exhaustiveTests; i++ ))
		do
		   	echo "running $directory/$exe with $exhaustiveFolder$i.in and $exhaustiveFolder$i.out"
		   	./sortTestInPosOrNeg.sh $directory/$exe $exhaustiveFolder$i.in $exhaustiveFolder$i.out "-exhaustive"
		done	
	fi

	for (( i=1; i<=$blackTests; i++ ))
	do
	   	echo "running $directory/$exe with $bbtestFolder$i.in and $bbtestFolder$i.out"
	   	./sortTestInPosOrNeg.sh $directory/$exe $bbtestFolder$i.in $bbtestFolder$i.out $testFilesPostfix
	
	
	done

	for (( i=1; i<=$whiteTests; i++ ))
	do
		echo "running $directory/$exe with $wbtestFolder$i.in and $wbtestFolder$i.out"
		./sortTestInPosOrNeg.sh $directory/$exe $wbtestFolder$i.in $wbtestFolder$i.out $testFilesPostfix

	done
done
