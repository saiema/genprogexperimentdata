#!/bin/bash

# $1 = EXE
# $2 = in file
# $3 = out file

set -f

program=$1
inArgs="$(cat $2)"
expected="$(cat $3)"

cleanExpected=$(echo $expected | sed 's/\n//g' | sed 's/ //g')
#cleanExpected=$(echo $expected | tr '\n' ' ')

obtained="$(timeout --kill-after=2 2 $1 <<< $inArgs)"
cleanObtained=$(echo $obtained | sed 's/\n//g' | sed 's/ //g')
#cleanObtained=$(echo $obtained | tr '\n' ' ')

diff <(echo "$cleanExpected") <(echo "$cleanObtained")
#diff <(echo "$expected") <(echo "$obtained")
