#!/bin/bash

# $1 = EXE
# $2 = in file
# $3 = out file
# $4 = test postfix

set -f

program="$1"
inArgs="$(cat $2)"
expected="$(cat $3)"
postfix="$4"
cleanExpected=$(echo $expected | sed 's/\n//g' | sed 's/ //g')
#cleanExpected=$(echo $expected | tr '\n' ' ')
parentDir=$(dirname "$program")/

negTests=$parentDir"negTests$postfix.txt"
posTests=$parentDir"posTests$postfix.txt"

echo "negTests file : $negTests"
echo "posTests file : $posTests"

obtained="$(timeout --kill-after=2 2 $1 <<< $inArgs)"
cleanObtained=$(echo $obtained | sed 's/\n//g' | sed 's/ //g')
#cleanObtained=$(echo $obtained | tr '\n' ' ')

echo "clean expected : $cleanExpected"
echo "clean obtained : $cleanObtained"

#echo "expected : $expected"
#echo "obtained : $obtained"

DIFF=$(diff <(echo "$cleanExpected") <(echo "$cleanObtained"))
#DIFF=$(diff <(echo "$obtained") <(echo "$expected"))

if [ "$DIFF" != "" ]
then
	echo $DIFF
	echo "../../"$2 >> $negTests
else
	echo "../../"$2 >> $posTests
fi

set +f
