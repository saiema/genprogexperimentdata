#!/bin/bash

#$1 : program name (e.g.: median)
#$2 : suite folder (contains .in files)

set -f

program="$1"
tfolder="$2"

for t in $(ls $tfolder | awk '/[0-9]+\.in/'); do

	echo "Test: "$t
	fullPath=$tfolder$t	
	echo "Test full path: $fullPath"

	gcc -o "$program" "$program.c" -lm -fno-stack-protector

	inFile="$fullPath"
	outFile="${fullPath/.in/.out}"
	cp $outFile "$outFile.old"

	inArgs="$(cat $inFile)"
	obtained="$(timeout --kill-after=2 2 ./$program <<< $inArgs)"
	#cleanObtained=$(echo $obtained | tr '\n' ' ')
	cleanObtained=$(echo $obtained | sed 's/\n//g' | sed 's/ //g')
	echo "clean obtained: $cleanObtained"
	rm $outFile
	touch $outFile
	echo $obtained >> $outFile
	expected="$(cat $outFile.old)"	

	#cleanExpected=$(echo $expected | tr '\n' ' ')
	cleanExpected=$(echo $expected | sed 's/\n//g' | sed 's/ //g')
	echo "clean expected: $cleanExpected"
	
	DIFF=$(diff <(echo "$cleanObtained") <(echo "$cleanExpected"))
	if [ "$DIFF" != "" ]
	then
		echo "Difference found between $outFile and $outFile.old"
		echo $DIFF
	fi

done

set +f
