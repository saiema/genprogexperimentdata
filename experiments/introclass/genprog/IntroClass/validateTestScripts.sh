#!/bin/bash


#====================FUNCTIONS==============================

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

#===========================================================


#========================MAIN===============================

#$1 : The program to verify (checksum, digits, grade, syllables, smallest, median)

program="$1"
iterations="$2"
testingScript="test.sh"

cd $1

for f in $(find . | awk -v pat="$program.c" '$0 ~ pat'); do

	echo "File: "$f
	directory="$(dirname "$f")"
	echo "Directory: "$directory
	pushd $directory

	matchingNumbers=$(echo ${directory:(-3)} | grep -o "[0-9]")
	if [[ -z  $matchingNumbers ]]; then
		echo "skipping tests folder variant"
		popd
		continue
	fi


	if [ ! -e $testingScript ]; then
		echo "Missing testing script: $testingScript"	
		exit 1
	fi	
	
	posTests=$(grep -c  "^p[0-9]" $testingScript)
	negTests=$(grep -c  "^n[0-9]" $testingScript)

	echo "Positive tests: $posTests"
	echo "Negative tests: $negTests"

	if [[ $posTests == 0 && $negTests == 0 ]]; then
		echo "Incorrect script format, no pi)/ni) lines found"
		exit 1
	fi

	if [[ $negTests == 0 ]]; then
		echo "skipping case with 0 negative tests"
		popd
		continue
	fi

	gcc -o "$program" "$program.c" -lm -fno-stack-protector

	for (( count=1; count<=$iterations; count++ ))
	do
		echo "Checking $posTests positive tests (script exit code should be 0) [iteration $count]"
		for (( t=1; t<=$posTests; t++ ))
		do
			./$testingScript "./$program" "p$t" > /dev/null
			exitCode="$?"
			if [[ "$exitCode" -ne "0" ]]; then
				echo "Positive test $t didn't pass, $testingScript returned $exitCode instead of 0"
				exit 1		
			fi
		done
		echo "OK"

		echo "Checking $negTests negative tests (script exit code should be 1) [iteration $count]"
		for (( t=1; t<=$negTests; t++ ))
		do
			./$testingScript "./$program" "n$t" > /dev/null
			exitCode="$?"
			if [[ "$exitCode" -ne "1" ]]; then
				echo "Negative test $t didn't pass, $testingScript returned $exitCode instead of 1"
				exit 1		
			fi
		done
		echo "OK"
	done

	popd

done
