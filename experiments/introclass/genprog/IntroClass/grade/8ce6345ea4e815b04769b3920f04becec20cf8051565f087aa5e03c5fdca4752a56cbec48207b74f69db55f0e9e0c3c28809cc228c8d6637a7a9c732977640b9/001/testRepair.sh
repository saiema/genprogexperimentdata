#!/bin/bash
#"__TEST_SCRIPT__ __EXE_NAME__ __TEST_NAME__ __PORT__ __SOURCE_NAME__ __FITNESS_FILE__ 1>/dev/null 2>/dev/null"

# $1 EXE (runScript?, for now it will use the Main's name without the .c or the ./)
# $2 [p|n]X
# - port, ignored
# - source name (the file to repair?, ignored)
# - fitness file, ignored

#DON'T MODIFY THIS SCRIPT OR YOU WILL SUFFER MY WRATH!

ORIGINAL=0
EXHAUSTIVE=1

program=$1
testArg=$2
testNum=${testArg:1}
testType=${testArg:0:1}
suiteType=$3

customTestsPostfixExhaustive="-exhaustive"
customTestsPostfixOriginal="-original"

testFile=""
if [[ "$suiteType" -eq "$ORIGINAL" ]]; then
	testFile="$customTestsPostfixOriginal.txt"
elif [[ "$suiteType" -eq "$EXHAUSTIVE" ]]; then
	testFile="$customTestsPostfixExhaustive.txt"
else
	echo "the suite type must be EXHAUSTIVE(1) or ORIGINAL(0), $suiteType is an invalid input"
	exit 1
fi

if [ "$testType" == "p" ]; then
	testFile="posTests$testFile"
elif [ "$testType" == "n" ]; then
	testFile="negTests$testFile"
else
	echo "the test type must be positive(p) or negative(n), $testType is an invalid input"
	exit 1
fi

testInLine=$(tail -n +$testNum $testFile | head -n1)

testOutLine="${testInLine/.in/.out}"

./runWithInOut.sh $1 $testInLine $testOutLine
