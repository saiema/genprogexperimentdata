#!/bin/bash

#$1 : program name (e.g.: median)
#$2 : test files original postfix (e.g.: -original)

outputFile="test.sh"
program=$1
testFilesOriginalPostFix=$2
minOutput=$3
deleteInvalidCases="$4"
genprogScriptsFolder=$5

genprogScriptPrefix="runGenProgExperiment"

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

report () {
	reportOriginal=$1
	reportExhaustive=$2
	variant=$3
	shortOutput=$4
	output="Variant $variant - "	
	if [[ "$reportOriginal" -ne "0" && "$shortOutput" -eq "0" ]]; then
		output=$output'\n''no negative original tests found'
	elif [[ "$reportOriginal" -ne "0" ]]; then
		output=$output" Orig Neg "	
	fi
	if [[ "$reportExhaustive" -ne "0" && "$shortOutput" -eq "0" ]]; then
		output=$output'\n''no negative exhaustive tests found'	
	elif [[ "$reportExhaustive" -ne "0" ]]; then
		output=$output" Exh Neg "	
	fi
	if [[ "$reportExhaustive" -ne "0" && "$reportOriginal" -eq "0"  && "$shortOutput" -eq "0" ]]; then
		output=$output'\n''CAUTION: original negative tests found but there are no negative exhaustive tests!!!'
	elif [[ "$reportExhaustive" -ne "0" && "$reportOriginal" -eq "0" ]]; then
		output=$output" CAUTION!"	
	fi
	echo -e $output
}

echo "Verifying $program tests"

pushd $program

for f in $(find . | awk -v pat="$program.c" '$0 ~ pat'); do

	#echo "File: "$f
	directory="$(dirname "$f")"
	#echo "Directory: "$directory
	pushd $directory

	variantID=""	
	mathingNumbers=$(echo ${directory:(-3)} | grep -o "[0-9]")
	if [[ -z  $mathingNumbers ]]; then
		echo "skipping tests folder variant"
		popd
		continue
	else
		varDir=$(echo $directory | sed "s/\.\///g")
		variantID="${varDir:0:7}-${varDir:(-3)}"	
	fi	
	
	negTestoriginal="negTests$testFilesOriginalPostFix.txt"
	negTestexhaustive="negTests-exhaustive.txt"

	numNTO=0
	if [ -f $negTestoriginal ]; then
		numNTO=$(wc -l < $negTestoriginal)		
	fi
	numNTE=0
	if [ -f $negTestexhaustive ]; then
		numNTE=$(wc -l < $negTestexhaustive)		
	fi

	nNTO=0
	if [[ "$numNTO" -eq "0" ]]; then
		nNTO=1		
	fi	
	
	nNTE=0
	if [[ "$numNTE" -eq "0" ]]; then
		nNTE=1			
	fi

	if [[ "$nNTO" -eq "1" || "$nNTE" -eq "1" ]]; then
		report $nNTO $nNTE $variantID $minOutput
	fi

	popd

	if [[ "$nNTO" -eq "1" && "$nNTE" -eq "1" && "$deleteInvalidCases" -eq "1" ]]; then
		scriptToDelete=$genprogScriptsFolder$genprogScriptPrefix$variantID".sh"
		echo "Deleting script : $scriptToDelete"
		rm $scriptToDelete
	fi

done

popd
