#!/bin/bash


originalFiles=$(git ls-files -z -m | xargs -I {} -0 echo {} | awk '/experimentsW/')

for f in $originalFiles; do
	echo $f
	diffLines=$(git diff --shortstat $f)
	#echo $diffLines
	if [[ $diffLines =~ .*10.*insertions.*10.*deletions.* ]]; then
		echo "Has 10 modifications"
		copy=$(echo "$f" | sed 's/WB/BB/g')
		echo "Will be copied to"
		echo $copy
		cp $f $copy
	else
		echo "Has less than 10 modifications"
		copy=$(echo "$f" | sed 's/WB/TAINTED/g')
		echo "Will be copied to"
		echo $copy
		cp $f $copy
	fi
	#echo "Will be copied to"
	#destDir=$(dirname $f | sed 's/WB/verdaderosWB/g')"/"
	#mkdir -p $destDir
	#cp $f $destDir
	#echo $destDir
done
